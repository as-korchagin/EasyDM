package korchagin.as.easydm;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class AgreementHistoryActivity extends AppCompatActivity {

    private int doc_id;
    private ArrayList<Agreement> agreement_list;
    private AgreementHistoryRVAdapter rvAdapter;
    private RecyclerView mainRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agreement_history_layout);
        Intent caller = getIntent();
        agreement_list = new ArrayList();
        mainRV = findViewById(R.id.history_recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        mainRV.setLayoutManager(llm);
        doc_id = caller.getIntExtra("doc_id", -1);
        if (doc_id == -1) {
            Toast.makeText(getBaseContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            finish();
        }
        initHistory();
    }

    private void initHistory() {
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("document_id", Integer.toString(doc_id))
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.server_url) + getString(R.string.get_agreement_history))
                .post(requestBody)
                .build();
        Downloader downloader = new Downloader(request);
        try {
            String json_response = downloader.execute().get();
            JSONObject object = new JSONObject(json_response);
            if (object.getInt("error") == 0) {
                JSONObject response = object.getJSONObject("response");
                for (Iterator iterator = response.keys(); iterator.hasNext(); ) {
                    String revision_id = (String) iterator.next();
                    agreement_list.add(new Agreement(doc_id, Integer.parseInt(revision_id),
                            response.getJSONObject(revision_id)));
                }
                Collections.sort(agreement_list, new HistorySorter());
                rvAdapter = new AgreementHistoryRVAdapter(agreement_list);
                mainRV.setAdapter(rvAdapter);
            } else {
                Toast.makeText(getBaseContext(), "Can not download agreement history", Toast.LENGTH_SHORT).show();
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class AgreementHistoryRVAdapter extends RecyclerView.Adapter<AgreementHistoryRVAdapter.HistoryVH> {

        private ArrayList<Agreement> agreement_history;

        public AgreementHistoryRVAdapter(ArrayList<Agreement> agreement_history) {
            this.agreement_history = agreement_history;
        }

        @Override
        public HistoryVH onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_item_card, viewGroup, false);
            return new HistoryVH(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(HistoryVH historyVH, int i) {
            if (i == 0) {
                historyVH.note_header.setText(historyVH.note_header.getText() + " (Текущая версия)");
            }
            historyVH.note.setText(agreement_history.get(i).getNote());
            historyVH.sender_email.setText(historyVH.sender_email.getText() + agreement_history.get(i).getSender_email());
            historyVH.sender_name.setText(historyVH.sender_name.getText() + agreement_history.get(i).getSender_name());
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        @Override
        public int getItemCount() {
            return agreement_history.size();
        }

        class HistoryVH extends RecyclerView.ViewHolder {

            CardView cardView;
            TextView note;
            TextView sender_email;
            TextView sender_name;
            TextView note_header;

            public HistoryVH(View itemView) {
                super(itemView);
                cardView = itemView.findViewById(R.id.history_cv);
                note = itemView.findViewById(R.id.agreement_note);
                sender_email = itemView.findViewById(R.id.agreement_sender_email);
                sender_name = itemView.findViewById(R.id.agreement_sender_name);
                note_header = itemView.findViewById(R.id.note_header);
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent viewer_intent = new Intent(AgreementHistoryActivity.this, AgreementViewer.class);
                        viewer_intent.putExtra("document_id", agreement_history.get(getAdapterPosition()).getDocument_id());
                        viewer_intent.putExtra("revision", agreement_history.get(getAdapterPosition()).getRevision());
                        startActivity(viewer_intent);
                    }
                });
            }
        }
    }
}

class HistorySorter implements Comparator<Agreement> {

    @Override
    public int compare(Agreement agreement, Agreement t1) {
        return t1.getRevision() - agreement.getRevision();
    }
}