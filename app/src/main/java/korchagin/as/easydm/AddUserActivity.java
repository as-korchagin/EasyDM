package korchagin.as.easydm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.*;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AddUserActivity extends AppCompatActivity {

    private EditText user_name;
    private EditText user_email;
    private EditText user_login;
    private EditText user_pass;
    private EditText user_phone;
    private Spinner user_position;
    private Button add_user_btn;
    private Map<String, String> position_types;
    private String selected_position_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_user_layout);
        user_email = findViewById(R.id.employee_email);
        user_phone = findViewById(R.id.employee_phone);
        user_name = findViewById(R.id.employee_name);
        user_login = findViewById(R.id.employee_login);
        user_pass = findViewById(R.id.employee_pass);
        user_position = findViewById(R.id.employee_position);
        add_user_btn = findViewById(R.id.add_user_btn);
        add_user_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pushNewUser()) {
                    Toast.makeText(getBaseContext(), "User added successfully!", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "Can not add new user, check internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        position_types = new HashMap<>();
        initPositions();
        ArrayAdapter<?> spinner_adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_spinner_item, new ArrayList<>(this.position_types.keySet()));
        user_position.setAdapter(spinner_adapter);
        user_position.setSelection(1);
        user_position.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selected_position_type = position_types.get(adapterView.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initPositions() {
        Request request = new Request.Builder()
                .url(getString(R.string.server_url) + getString(R.string.position_types))
                .build();
        Downloader downloader = new Downloader(request);
        try {
            JSONObject response = new JSONObject(downloader.execute().get());
            response = response.getJSONObject("response");
            for (Iterator iterator = response.keys(); iterator.hasNext(); ) {
                String key = (String) iterator.next();
                this.position_types.put(response.getString(key), key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean pushNewUser() {
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("name", user_name.getText().toString())
                .addFormDataPart("email", user_email.getText().toString())
                .addFormDataPart("login", user_login.getText().toString())
                .addFormDataPart("pass", user_pass.getText().toString())
                .addFormDataPart("phone", user_phone.getText().toString())
                .addFormDataPart("position_id", selected_position_type)
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.server_url) + getString(R.string.add_user))
                .post(requestBody)
                .build();
        Downloader downloader = new Downloader(request);
        try {
            JSONObject response = new JSONObject(downloader.execute().get());
            return response.getInt("error") == 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
