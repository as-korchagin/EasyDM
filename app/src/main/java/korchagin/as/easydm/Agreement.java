package korchagin.as.easydm;

import org.json.JSONObject;

public class Agreement {

    private String sender_email;
    private String sender_name;
    private int revision;
    private int document_id;
    private String note;

    Agreement(int doc_id, int revision_id, JSONObject object) {
        this.document_id = doc_id;
        this.revision = revision_id;
        try {
            this.sender_email = object.getString("sender_email");
            this.sender_name = object.getString("sender_name");
            this.note = object.getString("note");
        } catch (Exception e) {
            this.note = "Parsing Error";
            e.printStackTrace();
        }
    }

    public String getSender_email() {
        return sender_email;
    }

    public String getSender_name() {
        return sender_name;
    }

    public int getRevision() {
        return revision;
    }

    public int getDocument_id() {
        return document_id;
    }

    public String getNote() {
        return note;
    }
}
