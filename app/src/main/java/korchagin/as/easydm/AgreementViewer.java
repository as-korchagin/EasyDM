package korchagin.as.easydm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import jp.wasabeef.richeditor.RichEditor;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.json.JSONObject;

public class AgreementViewer extends AppCompatActivity {

    private int revision;
    private int document_id;
    private RichEditor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agreement_revision_viewer);
        Intent intent = getIntent();
        revision = intent.getIntExtra("revision", -1);
        document_id = intent.getIntExtra("document_id", -1);
        editor = findViewById(R.id.agreement_viewer_editor);
        if (revision == -1 || document_id == -1) {
            Toast.makeText(getBaseContext(), "Wrong data received by intent", Toast.LENGTH_SHORT).show();
            finish();
        }
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("revision", Integer.toString(revision))
                .addFormDataPart("document_id", Integer.toString(document_id))
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.server_url) + getString(R.string.get_agreement_revision))
                .post(requestBody)
                .build();
        Downloader downloader = new Downloader(request);

        try {
            JSONObject response = new JSONObject(downloader.execute().get());
            if (response.getInt("error") != 0) {
                Toast.makeText(getBaseContext(), "Wrong data received", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                editor.setHtml(response.getString("response"));
            }
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), "An error occurred when downloading agreement revision", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

}
