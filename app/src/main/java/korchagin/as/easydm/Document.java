package korchagin.as.easydm;

import org.json.JSONException;
import org.json.JSONObject;

class Document {

    private int id;
    private String document_name;
    private String customer_name;
    private String customer_email;
    private String create_date;
    private int agreement_id;

    Document(int id, JSONObject document) {
        this.id = id;
        try {
            this.document_name = document.getString("doc_name");
            this.customer_name = document.getString("customer");
            this.customer_email = document.getString("customer_email");
            this.create_date = document.getString("create_date");
            this.agreement_id = document.getInt("agreement_id");
        } catch (JSONException e) {
            this.document_name = "Parse error";
        }
    }

    public int getId() {
        return this.id;
    }

    public int getAgreementId() {
        return this.agreement_id;
    }

    public String getDocName() {
        return this.document_name;
    }

    public String getCustomer() {
        return this.customer_name;
    }

    public String getCustomerEmail() {
        return this.customer_email;
    }

    public String getCreateDate() {
        return this.create_date;
    }
}