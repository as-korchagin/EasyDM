package korchagin.as.easydm;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.json.JSONObject;

import static korchagin.as.easydm.Constants.*;

public class LoginActivity extends AppCompatActivity {

    private EditText mEmailView;
    private EditText mPasswordView;
    private ProgressBar mProgressView;
    private View mLoginFormView;
    private SharedPreferences easydmSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        easydmSettings = getSharedPreferences(getString(R.string.preferences_name), Context.MODE_PRIVATE);
        if (isAuthenticated()) {
            startFileExplorer();
        }
        setContentView(R.layout.activity_login);
        mEmailView = findViewById(R.id.email);
        mPasswordView = findViewById(R.id.password);
        Button mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private boolean isAuthenticated() {
        return easydmSettings.getBoolean("auth_ok", false);
    }

    private void saveAuth(String email, int id, int is_admin) {
        SharedPreferences.Editor editor = easydmSettings.edit();
        editor.putBoolean("auth_ok", true);
        editor.putInt("is_admin", is_admin);
        editor.putString("email", email);
        editor.putInt("id", id);
        editor.apply();
    }

    private void startFileExplorer() {
        Intent fileExplorerIntent = new Intent(LoginActivity.this, FileExplorerActivity.class);
        startActivityForResult(fileExplorerIntent, START_FILE_EXPLORER_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == START_FILE_EXPLORER_ACTIVITY) {
            switch (resultCode) {
                case STOP_APPLICATION:
                    finish();
                    break;
                case EXIT_PROFILE:
                    SharedPreferences.Editor editor = easydmSettings.edit();
                    editor.clear();
                    editor.apply();
                    break;
            }
        }
    }

    private void attemptLogin() {
        mEmailView.setError(null);
        mPasswordView.setError(null);
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            Authentication userAuth = new Authentication(email, password);
            if (userAuth.isCredentialsValid()) {
                saveAuth(email, userAuth.getId(), userAuth.getIs_admin());
                startFileExplorer();
            }
            showProgress(false);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    public class Authentication {

        private String email;
        private String password;
        private int id;
        private int is_admin;

        Authentication(String email, String password) {
            this.email = email;
            this.password = password;
        }


        public boolean isCredentialsValid() {
            boolean auth_ok = false;
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("email", this.email)
                    .addFormDataPart("pass", this.password)
                    .build();
            Request request = new Request.Builder()
                    .url(getString(R.string.server_url) + getString(R.string.auth))
                    .addHeader("Content-Type", "text; charset=utf-8")
                    .post(requestBody)
                    .build();
            Downloader downloader = new Downloader(request);
            try {
                String json_response = downloader.execute().get();
                JSONObject response = new JSONObject(json_response);
                if (response.getJSONObject("response").getString("auth_ok").equals("true")) {
                    auth_ok = true;
                    this.id = response.getJSONObject("response").getInt("employee_id");
                    this.is_admin = response.getJSONObject("response").getInt("admin");
                } else {
                    Toast.makeText(getBaseContext(), response.getString("error"), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return auth_ok;
        }

        public int getId() {
            return this.id;
        }

        public int getIs_admin() {
            return this.is_admin;
        }

    }
}

