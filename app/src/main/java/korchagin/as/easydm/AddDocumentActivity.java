package korchagin.as.easydm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.*;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AddDocumentActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText document_name;
    private EditText document_desc;
    private Spinner spinner;
    private Button submit;
    private String selected_doc_type;
    private Map<String, String> doc_types;
    private SharedPreferences easydm_settings;
    private int employee_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_document_layout);
        Intent intent = getIntent();
        employee_id = intent.getIntExtra("employee_id", -1);
        doc_types = new HashMap<>();
        document_name = findViewById(R.id.document_name);
        document_desc = findViewById(R.id.document_desc);
        spinner = findViewById(R.id.spinner);
        submit = findViewById(R.id.submit);
        easydm_settings = getSharedPreferences(getString(R.string.preferences_name), Context.MODE_PRIVATE);
        submit.setOnClickListener(this);
        Request request = new Request.Builder()
                .url(getString(R.string.server_url) + getString(R.string.doc_types))
                .build();
        try {
            String json_response = new Downloader(request).execute().get();
            JSONObject object = new JSONObject(json_response);
            if (object.getInt("error") == 1) {
                throw new IOException("Wrong data received");
            } else {
                JSONObject doc_types = object.getJSONObject("response");
                for (Iterator iterator = doc_types.keys(); iterator.hasNext(); ) {
                    String key = (String) iterator.next();
                    this.doc_types.put(doc_types.getString(key), key);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayAdapter<?> adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_spinner_item, new ArrayList<>(this.doc_types.keySet()));
        spinner.setAdapter(adapter);
        spinner.setSelection(1);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    selected_doc_type = doc_types.get(adapterView.getSelectedItem());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit:
                if (!document_name.getText().toString().equals("") & !document_desc.getText().toString().equals("")) {
                    Intent intent = new Intent();
                    try {
                        RequestBody requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("doc_desc", document_desc.getText().toString())
                                .addFormDataPart("doc_name", document_name.getText().toString())
                                .addFormDataPart("doc_type_id", selected_doc_type)
                                .addFormDataPart("employee_id", Integer.toString(employee_id))
                                .build();
                        Request request = new Request.Builder()
                                .url(getString(R.string.server_url) + getString(R.string.add_doc))
                                .post(requestBody)
                                .build();

                        new Downloader(request).execute().get();
                        intent.putExtra("doctype_id", selected_doc_type);
                        setResult(RESULT_OK, intent);
                    } catch (Exception e) {
                        setResult(RESULT_CANCELED);
                        e.printStackTrace();
                    }
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "Заполните все поля", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
