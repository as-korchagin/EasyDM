package korchagin.as.easydm;

import android.os.AsyncTask;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class Downloader extends AsyncTask<Void, Void, String> {

    private OkHttpClient httpClient;
    private Request request;


    public Downloader(Request request) {
        this.request = request;
        httpClient = new OkHttpClient();
    }

    @Override
    protected String doInBackground(Void... voids) {
        String body = "{'error': 1}";
        try {
            Response response = this.httpClient.newCall(request).execute();
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected response " + response);
            }

            body = response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return body;
    }
}
