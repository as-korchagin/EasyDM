package korchagin.as.easydm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.ProgressBar;
import android.widget.TextView;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import static korchagin.as.easydm.Constants.EXIT_PROFILE;
import static korchagin.as.easydm.Constants.STOP_APPLICATION;

public class FileExplorerActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private final static int NOT_AGREED_DOCUMENTS = 0;
    private final static int AGREED_DOCUMENTS = 1;
    private ArrayList<Document> documents_list;
    private ProgressBar docs_download_bar;
    private String user_email;
    private SharedPreferences easydmSettings;
    private int is_admin;
    private RVAdapter adapter;
    private RecyclerView mainRV;
    private SwipeRefreshLayout refresh_layout;
    private int current_agreement_state = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        easydmSettings = getSharedPreferences(getString(R.string.preferences_name), Context.MODE_PRIVATE);
        user_email = easydmSettings.getString("email", "Error");
        is_admin = easydmSettings.getInt("is_admin", 0);
        setContentView(R.layout.file_explorer_activity);
        docs_download_bar = findViewById(R.id.docs_list_bar);
        mainRV = findViewById(R.id.recycler_view);
        LinearLayoutManager layoutMgr = new LinearLayoutManager(getApplicationContext());
        mainRV.setLayoutManager(layoutMgr);
        refresh_layout = findViewById(R.id.swipe_to_refresh);
        refresh_layout.setOnRefreshListener(this);
        initDocs(NOT_AGREED_DOCUMENTS);

    }

    @Override
    protected void onStart() {
        super.onStart();
        initDocs(current_agreement_state);
    }

    public void initDocs(int which_to_download) {
        String destination = which_to_download == NOT_AGREED_DOCUMENTS ? getString(R.string.docs_folder) : getString(R.string.agreed_docs_folder);
        documents_list = new ArrayList<>();
        docs_download_bar.setVisibility(View.VISIBLE);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("email", user_email)
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.server_url) + destination)
                .post(requestBody)
                .build();
        Downloader docs_downloader = new Downloader(request);
        try {
            String json_response = docs_downloader.execute().get();
            JSONObject object = new JSONObject(json_response);
            if (object.getInt("error") == 1) {
                throw new IOException("Wrong data received");
            } else {
                JSONObject docs = object.getJSONObject("response");
                for (Iterator iterator = docs.keys(); iterator.hasNext(); ) {
                    String key = (String) iterator.next();
                    documents_list.add(new Document(Integer.parseInt(key), docs.getJSONObject(key)));
                }
                Collections.sort(documents_list, new DocumentSorter());
            }
            adapter = new RVAdapter(documents_list, getBaseContext());
            mainRV.setAdapter(adapter);
            if (refresh_layout.isRefreshing()) {
                refresh_layout.setRefreshing(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (is_admin == 0) {
            getMenuInflater().inflate(R.menu.explorer_menu_not_admin, menu);
        } else if (is_admin == 1) {
            getMenuInflater().inflate(R.menu.explorer_menu_admin, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_document:
                Intent startAddDocument = new Intent(FileExplorerActivity.this, AddDocumentActivity.class);
                startAddDocument.putExtra("employee_id", easydmSettings.getInt("id", -1));
                startActivityForResult(startAddDocument, 1);
                return true;
            case R.id.exit_profile:
                setResult(EXIT_PROFILE, getIntent());
                finish();
                return true;
            case R.id.add_user:
                Intent start_add_user_activity = new Intent(FileExplorerActivity.this, AddUserActivity.class);
                startActivity(start_add_user_activity);
            case R.id.show_agreed:
                if (!item.isChecked()) {
                    current_agreement_state = AGREED_DOCUMENTS;
                    item.setChecked(true);
                    initDocs(AGREED_DOCUMENTS);
                } else {
                    current_agreement_state = NOT_AGREED_DOCUMENTS;
                    item.setChecked(false);
                    initDocs(NOT_AGREED_DOCUMENTS);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            initDocs(current_agreement_state);
        }
    }

    @Override
    public void onBackPressed() {
        Intent finish_intent = getIntent();
        setResult(STOP_APPLICATION, finish_intent);
        finish();
    }

    @Override
    public void onRefresh() {
        initDocs(current_agreement_state);
    }

    public class RVAdapter extends RecyclerView.Adapter<RVAdapter.DocumentVH> {

        private ArrayList<Document> documents;
        private Context context;

        RVAdapter(ArrayList<Document> documents, Context context) {
            this.documents = documents;
            this.context = context;
        }

        @Override
        public void onBindViewHolder(DocumentVH documentVH, int i) {
            documentVH.document_name.setText(documents.get(i).getDocName());
            documentVH.customer_name.setText(documents.get(i).getCustomer());
        }

        @Override
        public int getItemCount() {
            return documents.size();
        }

        @Override
        public DocumentVH onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_template_layout, viewGroup, false);
            return new DocumentVH(view);
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        class DocumentVH extends RecyclerView.ViewHolder {
            CardView cv;
            TextView document_name;
            TextView customer_name;

            DocumentVH(View itemView) {
                super(itemView);
                cv = itemView.findViewById(R.id.document_cv);
                document_name = itemView.findViewById(R.id.document_name);
                customer_name = itemView.findViewById(R.id.cusomer_name);
                cv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent edit_doument = new Intent(context, DocumentEditActivity.class);
                        edit_doument.putExtra("doc_id", documents_list.get(getAdapterPosition()).getId());
                        edit_doument.putExtra("employee_email", documents_list.get(getAdapterPosition()).getCustomerEmail());
                        startActivity(edit_doument);

                    }
                });
            }

        }
    }
}

class DocumentSorter implements Comparator<Document> {

    @Override
    public int compare(Document document, Document t1) {
        return t1.getAgreementId() - document.getAgreementId();
    }
}
