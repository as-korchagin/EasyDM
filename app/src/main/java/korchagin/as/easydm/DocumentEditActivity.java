package korchagin.as.easydm;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener;
import jp.wasabeef.richeditor.RichEditor;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.json.JSONObject;


public class DocumentEditActivity extends AppCompatActivity implements View.OnClickListener, ColorPickerDialogListener, View.OnLongClickListener {

    private RichEditor editor;
    private ImageButton text_color_btn;
    private ImageButton text_background_btn;
    private int text_color;
    private int text_background_color;
    private int currentColorId;
    private int doc_id;
    private String customer_email;
    private String current_user_email;
    private int current_user_id;
    private String receiver_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.document_edit_layout);
        editor = findViewById(R.id.editor);
        text_color = Color.parseColor("#000000");
        text_background_color = Color.parseColor("#000000");

        text_color_btn = findViewById(R.id.text_color);
        text_background_btn = findViewById(R.id.text_back_color);

        findViewById(R.id.bold).setOnClickListener(this);
        findViewById(R.id.italic).setOnClickListener(this);
        findViewById(R.id.indent).setOnClickListener(this);
        findViewById(R.id.outdent).setOnClickListener(this);
        findViewById(R.id.strikethrough).setOnClickListener(this);
        findViewById(R.id.underline).setOnClickListener(this);
        findViewById(R.id.align_left).setOnClickListener(this);
        findViewById(R.id.align_center).setOnClickListener(this);
        findViewById(R.id.align_right).setOnClickListener(this);
        findViewById(R.id.undo).setOnClickListener(this);
        findViewById(R.id.redo).setOnClickListener(this);
        text_color_btn.setOnClickListener(this);
        text_background_btn.setOnClickListener(this);


        findViewById(R.id.text_color).setOnLongClickListener(this);
        findViewById(R.id.text_back_color).setOnLongClickListener(this);

        SharedPreferences easydmprefs = getSharedPreferences(getString(R.string.preferences_name), Context.MODE_PRIVATE);
        current_user_email = easydmprefs.getString("email", "error");
        current_user_id = easydmprefs.getInt("id", -1);

        Intent intent = getIntent();
        doc_id = intent.getIntExtra("doc_id", -1);
        customer_email = intent.getStringExtra("employee_email");
        if (doc_id != -1) {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("doc_id", Integer.toString(doc_id))
                    .build();
            Request request = new Request.Builder()
                    .url(getString(R.string.server_url) + getString(R.string.get_doc_html))
                    .post(requestBody)
                    .build();
            Downloader downloader = new Downloader(request);
            try {
                String result = downloader.execute().get();
                if (!result.equals("")) {
                    JSONObject response = new JSONObject(result);
                    if (response.getInt("error") != 1) {
                        editor.setHtml(response.getString("response"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getBaseContext(), "Cannot open document", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.bold:
                editor.setBold();
                break;
            case R.id.italic:
                editor.setItalic();
                break;
            case R.id.indent:
                editor.setIndent();
                break;
            case R.id.outdent:
                editor.setOutdent();
                break;
            case R.id.undo:
                editor.undo();
                break;
            case R.id.redo:
                editor.redo();
                break;
            case R.id.strikethrough:
                editor.setStrikeThrough();
                break;
            case R.id.underline:
                editor.setUnderline();
                break;
            case R.id.align_left:
                editor.setAlignLeft();
                break;
            case R.id.align_center:
                editor.setAlignCenter();
                break;
            case R.id.align_right:
                editor.setAlignRight();
                break;
            case R.id.text_color:
                editor.setTextColor(text_color);
                break;
            case R.id.text_back_color:
                editor.setTextBackgroundColor(text_background_color);
                break;
            default:
                break;
        }
    }

    @Override
    public void onColorSelected(int i, int i1) {
        switch (currentColorId) {
            case R.id.text_color:
                text_color_btn.setColorFilter(i1);
                text_color = i1;
                break;
            case R.id.text_back_color:
                text_background_btn.setColorFilter(i1);
                text_background_color = i1;
                break;
            default:
                break;
        }
    }

    @Override
    public void onDialogDismissed(int i) {

    }

    @Override
    public boolean onLongClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.text_color:
                ColorPickerDialog.newBuilder().show(this);
                currentColorId = R.id.text_color;
                break;
            case R.id.text_back_color:
                ColorPickerDialog.newBuilder().show(this);
                currentColorId = R.id.text_back_color;
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.editor_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.send_to_agreement:
                if (customer_email.equals(current_user_email)) {
                    showReceiverDialog();
                }
                return true;
            case R.id.save_document:
                if (!sendNewRevision(editor.getHtml(), current_user_email, "Saving document")) {
                    Toast.makeText(getBaseContext(), "Не удалось сохранить документ, попробуйте снова", Toast.LENGTH_SHORT).show();
                } else {
                    finish();
                }
                return true;
            case R.id.agree_document:
                agreeDocument();
                return true;
            case R.id.get_note:
                String text = getNote();
                if (!text.equals("")) {
                    AlertDialog.Builder alert_builder = new AlertDialog.Builder(this);
                    LayoutInflater inflater = this.getLayoutInflater();
                    View noteDialog = inflater.inflate(R.layout.dialog_show_note, null);
                    final TextView noteText = noteDialog.findViewById(R.id.note_text);
                    noteText.setText(text);
                    alert_builder.setView(noteDialog)
                            .setTitle("Примечание")
                            .setPositiveButton("Закрыть", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                    AlertDialog dialog = alert_builder.create();
                    dialog.show();

                } else {
                    Toast.makeText(getBaseContext(), "Не удалось загрузить примечание, попробуйте позднее", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.show_history:
                get_agreement_history();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String getNote() {
        String result = "";
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("document_id", Integer.toString(doc_id))
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.server_url) + getString(R.string.get_note))
                .post(requestBody)
                .build();
        Downloader downloader = new Downloader(request);
        try {
            JSONObject response = new JSONObject(downloader.execute().get());
            if (response.getInt("error") == 0) {
                result = response.getString("response");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private void get_agreement_history() {
        Intent historyActivityIntent = new Intent(DocumentEditActivity.this, AgreementHistoryActivity.class);
        historyActivityIntent.putExtra("doc_id", doc_id);
        startActivity(historyActivityIntent);
    }

    private void showReceiverDialog() {
        AlertDialog.Builder alert_builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_set_receiver, null);
        final EditText receiver_email = dialogView.findViewById(R.id.dialog_receiver_email);
        final EditText agreement_note = dialogView.findViewById(R.id.dialog_note);
        alert_builder.setView(dialogView)
                .setTitle("Укажите получателя документа")
                .setPositiveButton("Отправить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (isReceiverExists(receiver_email.getText().toString())) {
                            if (!sendNewRevision(editor.getHtml(), receiver_email.getText().toString(), agreement_note.getText().toString())) {
                                Toast.makeText(getBaseContext(), "Не удалось обновить версию", Toast.LENGTH_SHORT).show();
                            } else {
                                finish();
                            }
                        } else {

                            Toast.makeText(getBaseContext(), "Пользователя с таким именем не существует", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("Отменить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        try {
            AlertDialog dialog = alert_builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean agreeDocument() {
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("document_id", Integer.toString(doc_id))
                .addFormDataPart("user_id", Integer.toString(current_user_id))
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.server_url) + getString(R.string.agree_doc))
                .post(requestBody)
                .build();
        Downloader downloader = new Downloader(request);
        try {
            JSONObject response = new JSONObject(downloader.execute().get());
            if (response.getInt("error") != 1) {
                finish();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isReceiverExists(String receiver) {
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("receiver_email", receiver)
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.server_url) + getString(R.string.is_receiver_exists))
                .post(requestBody)
                .build();
        Downloader downloader = new Downloader(request);
        try {
            JSONObject response = new JSONObject(downloader.execute().get());
            return response.getInt("error") == 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean sendNewRevision(String text, String receiver_email, String agreement_note) {
        if (text.equals("")) {
            text = "skfngs[okedvbo[ske[othns[oedfbaokmspejvmaoeurh jpaow awoihoawihvnawh[ocmiwmehciw";
        }
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("document_id", Integer.toString(doc_id))
                .addFormDataPart("customer_email", receiver_email)
                .addFormDataPart("sender", current_user_email)
                .addFormDataPart("agreement_note", agreement_note)
                .addFormDataPart("text", text)
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.server_url) + getString(R.string.add_new_revision))
                .post(requestBody)
                .build();
        Downloader downloader = new Downloader(request);
        try {
            JSONObject response = new JSONObject(downloader.execute().get());
            return response.getInt("error") != 1;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
